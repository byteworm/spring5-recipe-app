package com.blankbyte.spring5recipeapp.services;

import com.blankbyte.spring5recipeapp.commands.RecipeCommand;
import com.blankbyte.spring5recipeapp.converters.RecipeCommandToRecipe;
import com.blankbyte.spring5recipeapp.converters.RecipeToRecipeCommand;
import com.blankbyte.spring5recipeapp.domain.Recipe;
import com.blankbyte.spring5recipeapp.repositories.RecipeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RecipeServiceIT {
	private static final String NEW_DESCRITPION = "new description";

	@Autowired
	private RecipeRepository recipeRepository;

	@Autowired
	private RecipeToRecipeCommand recipeToRecipeCommand;

	@Autowired
	private RecipeService recipeService;

	@Autowired
	private RecipeCommandToRecipe recipeCommandToRecipe;

	@Test
	@Transactional
	public void testSaveOfDescription() throws Exception {
		//given
		//get first recipe
		Iterable<Recipe> recipes = recipeRepository.findAll();
		Recipe testRecipe = recipes.iterator().next();
		//since we save a command, we need the command version of the recipe
		RecipeCommand testRecipeCommand = recipeToRecipeCommand.convert(testRecipe);

		//when
		testRecipeCommand.setDescription(NEW_DESCRITPION);
		RecipeCommand savedRecipeCommand = recipeService.saveRecipeCommand(testRecipeCommand);

		//then
		assertEquals(NEW_DESCRITPION, savedRecipeCommand.getDescription());
		assertEquals(testRecipe.getId(), savedRecipeCommand.getId());
		assertEquals(testRecipe.getCategories().size(), savedRecipeCommand.getCategories().size());
		assertEquals(testRecipe.getIngredients().size(), savedRecipeCommand.getIngredients().size());
	}
}
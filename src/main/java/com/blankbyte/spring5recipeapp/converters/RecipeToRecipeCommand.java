package com.blankbyte.spring5recipeapp.converters;

import com.blankbyte.spring5recipeapp.commands.RecipeCommand;
import com.blankbyte.spring5recipeapp.domain.Recipe;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class RecipeToRecipeCommand implements Converter<Recipe, RecipeCommand> {

	private CategoryToCategoryCommand categoryConverter;
	private NotesToNotesCommand notesConverter;
	private IngredientToIngredientCommand ingredientConverter;

	public RecipeToRecipeCommand(CategoryToCategoryCommand categoryConverter, NotesToNotesCommand notesConverter, IngredientToIngredientCommand ingredientConverter) {
		this.categoryConverter = categoryConverter;
		this.notesConverter = notesConverter;
		this.ingredientConverter = ingredientConverter;
	}

	@Synchronized
	@Nullable
	@Override
	public RecipeCommand convert(Recipe recipe) {

		if (recipe == null) {
			return null;
		}

		RecipeCommand recipeCommand = new RecipeCommand();
		recipeCommand.setId(recipe.getId());
		recipeCommand.setCookTime(recipe.getCookTime());
		recipeCommand.setDescription(recipe.getDescription());
		recipeCommand.setDifficulty(recipe.getDifficulty());
		recipeCommand.setDirections(recipe.getDirections());
		recipeCommand.setPrepTime(recipe.getPrepTime());
		recipeCommand.setServings(recipe.getServings());
		recipeCommand.setSource(recipe.getSource());
		recipeCommand.setUrl(recipe.getUrl());
		recipeCommand.setNotes(notesConverter.convert(recipe.getNotes()));

		if (recipe.getCategories() != null && recipe.getCategories().size() > 0) {
			recipe.getCategories().forEach(category -> recipeCommand.getCategories().add(categoryConverter.convert(category)));
		}

		if (recipe.getIngredients() != null && recipe.getIngredients().size() > 0) {
			recipe.getIngredients().forEach(ingredient -> recipeCommand.getIngredients().add(ingredientConverter.convert(ingredient)));
		}

		return recipeCommand;
	}
}

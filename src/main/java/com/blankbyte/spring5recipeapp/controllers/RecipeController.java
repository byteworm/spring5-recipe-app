package com.blankbyte.spring5recipeapp.controllers;

import com.blankbyte.spring5recipeapp.commands.RecipeCommand;
import com.blankbyte.spring5recipeapp.services.RecipeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
public class RecipeController {

	private final RecipeService recipeService;

	public RecipeController(RecipeService recipeService) {
		this.recipeService = recipeService;
	}

	@GetMapping
	@RequestMapping("/recipe/{id}/show")
	public String showById(@PathVariable String id, Model model) {
		model.addAttribute("recipe", recipeService.findById(new Long(id)));

		return "recipe/show";
	}

	@GetMapping
	@RequestMapping("recipe/new")
	public String newRecipe(Model model) {
		model.addAttribute("recipe", new RecipeCommand());

		return "recipe/recipeform";
	}

	@GetMapping
	@RequestMapping("recipe/{id}/update")
	public String updateRecipe(@PathVariable String id, Model model) {
		RecipeCommand recipeCommand = recipeService.findCommandById(Long.valueOf(id));
		model.addAttribute("recipe", recipeCommand);

		return "recipe/recipeform";
	}

	@PostMapping
	@RequestMapping("recipe")
//	@RequestMapping(name = "recipe", method = RequestMethod.POST)
	public String saveOrUpdate(@ModelAttribute RecipeCommand command) {
		RecipeCommand recipeCommand = recipeService.saveRecipeCommand(command);

		return "redirect:/recipe/" + recipeCommand.getId() + "/show";
	}

	@GetMapping
	@RequestMapping("/recipe/{id}/delete")
	public String deleteById(@PathVariable String id) {
		log.debug("Deleting id: " + id);

		recipeService.deleteById(Long.valueOf(id));
		return "redirect:/";
	}
}
